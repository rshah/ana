// AStar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define WORKSPACE 1000

//00

std::map<Coord, Cell> env;
binaryHeap<std::pair<double, int>, std::map<Coord, Cell>::iterator> open, incons;

std::vector<int> w_space(WORKSPACE*WORKSPACE);
Coord wGoal;


Coord start;
std::map<Coord, Cell>::iterator goal_it;
std::map<std::pair<int,int>, int**> heuristicMap;
int G; 
int open_op;
int count;
double eps;
int iterationNumber;

clock_t startTime;

# define l0 145
# define l1 147
# define l2 147
# define l3 161
# define l4 151 

# define COST_BOUND 0


int **E;

/// BEGIN Environment Specific Functions

inline int round(double d)
{
  return (int) floor(d + 0.5);

}



inline int diff(char i, char j) {
  if(i == j) {
    return 0;
  }
  return 1;
}


inline void dp(int i, int j) {
  int m = proteins[i].length();
  int n = proteins[j].length();
  E = new int*[m];
  for(int x = 0; x < m; ++x)
    E[x] = new int[n];
  for(int a = m-1; a>=0; --a) {
    E[a][n-1] = (m-1) - a;
  }
  for(int b = n-1; b>=0; --b) {
    E[m-1][b] = (n-1) - b;
  }
  for(int a = m-2; a>=0; --a) {
    for(int b = n-2; b>=0; --b) {
      E[a][b] = std::min( std::min (E[a+1][b]+1, E[a][b+1]+1) , ( E[a+1][b+1] + diff(proteins[i].at(a),proteins[j].at(b))) );
    }
  }

  heuristicMap[std::make_pair(i,j)] = E;
}


inline void initw_space() {
  for (int i=0; i<NUM_SEQUENCES; ++i) {
    for(int j=i+1; j<NUM_SEQUENCES; ++j) {
      dp(i,j);
    }
  }		    
}


inline int getEdgeCost(const Coord& s, const Coord& n) {
	  
	int cost = 0;
	for (int i=0; i<NUM_SEQUENCES; ++i) {
		for(int j=i+1; j<NUM_SEQUENCES; ++j) {
			if(s.val[i] == n.val[i] || s.val[j] == n.val[j]) {
				cost+=2;
			}
			else if ((n.val[i] - s.val[i] == 1) && (n.val[j] - s.val[j] == 1)) {
				
        if (proteins[i].at(n.val[i]) == proteins[j].at(n.val[j])) {
					cost+=0;
				}
        
				else {
					cost+=1;
				}
			}
      else {
        cost+=INFTY;
      }
		}
	}
	return cost;
}



inline void init() {
  // BEGIN Environment Specific
  
  
  start = Coord(0,0,0,0,0);
	for(int i = 0; i<NUM_SEQUENCES; ++i) {
		wGoal.val[i] = proteins[i].size()-1; 
	}
  
  initw_space();
  // END Environment Specific

  goal_it = env.end();
  G = INFTY;
  open_op = 0;
  count = 0;
  iterationNumber = 0;
}


inline void createNeighbors(const Coord& s, std::vector<std::pair<Coord, int> >& neighbors) {
  for (int i0 = 0; i0 <= 1; ++i0) {
    for (int i1 = 0; i1 <= 1; ++i1) {
			for (int i2 = 0; i2 <= 1; ++i2) {
				for (int i3 = 0; i3 <= 1; ++i3) {
					for (int i4 = 0; i4 <= 1; ++i4) {
						Coord n(s.val[0] + i0, s.val[1] + i1, s.val[2] + i2, s.val[3] + i3, s.val[4] + i4);
						if (n.val[0] >= 0 && n.val[1] >= 0 && n.val[2] >= 0 && n.val[3] >= 0 && n.val[4] >= 0 
							&& n.val[0] <= wGoal.val[0] && n.val[1] <= wGoal.val[1] && n.val[2] <= wGoal.val[2] && n.val[3] <= wGoal.val[3] && n.val[4] <= wGoal.val[4] && n != s) {
								int x =	getEdgeCost(s, n);
								if(x>=0) {
									neighbors.push_back(std::make_pair(n, x));
								}
						}
					}
				}
			}
		}
	}
}
inline int h(const Coord& s) {
  int hCost = 0;
  int x,y;
  for (int i=0; i<NUM_SEQUENCES; ++i) {
		for(int j=i+1; j<NUM_SEQUENCES; ++j) {
      x=s.val[i];
      y=s.val[j];
      hCost += heuristicMap[std::make_pair(i,j)][x][y];
    }
  }
  return hCost;
}

inline bool isGoal(const Coord& s) {
  return (s == wGoal); 
}

/// END Environment Specific Functions

inline double e(std::map<Coord, Cell>::iterator s_it) {
  if (s_it->second.h == 0) {
    if (s_it->second.g >= G) {
      return 0;
    } else {
      return INFTY;
    }
  } else {
    return (G - s_it->second.g) / (double) s_it->second.h;
  }
}

inline double f(std::map<Coord, Cell>::iterator s_it) {
	return s_it->second.g + eps * s_it->second.h;
}

inline void publishSolution(double epsprime) {
	std::cout << "Suboptimality: " << epsprime << "\tTime: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << "\tCost: " << G << "\tExpands: " << count <<std::endl;



 /* std::cout << "Suboptimality: " << epsprime << std::endl;
  std::cout << "Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
  std::cout << "Cost: " << G << std::endl;
  std::cout << "Open: " << open.size() << std::endl;
  */
  //std::cout << "Count: " << count << std::endl;
  //std::cout << "Count2: " << -open_op << std::endl << std::endl;
  
  /*std::map<Coord, Cell>::iterator s_it = goal_it;
  while (s_it != env.end()) {
    std::cout << s_it->first << std::endl;
    s_it = s_it->second.bp;
  }
  std::cout << std::endl;*/
}

inline bool improvePathEKey() {
  while (!open.empty()) {
	  if(((clock() - startTime) / (double) CLOCKS_PER_SEC) > 1000) {
		  std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
		  exit(1);
	  }
    if (-open.key(0).first < eps) {
      eps = -open.key(0).first;
      //std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
    }
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
      
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
        // put or update n in open
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
          open.decreaseKey(n_it->second.pos_in_open, std::make_pair(-e(n_it), --open_op));
        } else { // not in OPEN, insert
          n_it->second.pos_in_open = open.insert(std::make_pair(-e(n_it), --open_op), n_it);
        }
	    }
		}
	} 
  return false;
}

inline void AStarEKey() {
  eps = INFTY;
  
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();
    
  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(-e(start_it), --open_op), start_it);
  
  while(!open.empty()) {

	  if(G > COST_BOUND) {
		  bool result = improvePathEKey();

		  // better path found
		  // update e-values (prune states with e-value <= 1) and reheapify 
		  // compute epsilon bound of path

		  double epsprime = 1;

		  for (size_t i = 0; i < open.size(); ) {
			  if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
				  epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
			  }

			  if (e(open[i]) <= 1) {
				  open[i]->second.pos_in_open = 0;
				  open.erase_unsafe(i);
			  } else {
				  open.changeKey_unsafe(i, std::make_pair(-e(open[i]), open.key(i).second));
				  ++i;
			  }
		  }
		  open.heapify();

		  // publish solution
		  if (result) {
			  //printf_s("Eps: %.4f  Time: %.3f \n", eps, (clock() - startTime) / (double) CLOCKS_PER_SEC);
			  publishSolution(epsprime);
		  }
	  }
	  else {
		  break;
	  }
  }
  std::cout << "Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
  }

inline bool improvePathInconsOpt() {
  ++iterationNumber;

  while (!open.empty()) { // && G >= open.key(0).first) {
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    //place that state s onto the closed list
		s_it->second.closed = iterationNumber;
    
    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
        // put or update n in open or incons
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        if (f(n_it) >= G) { // || n_it->second.closed == iterationNumber) { // Closed or not good enough for this iteration
          // Push on incons
          if (n_it->second.pos_in_incons != 0) { // already in INCONS, update e-value
            incons.decreaseKey(n_it->second.pos_in_incons, std::make_pair(-e(n_it), --open_op));
          } else {
            n_it->second.pos_in_incons = incons.insert(std::make_pair(-e(n_it), --open_op), n_it);
          }
				} else { // Not closed and good enough for this iteration
          // Push in in Open list
          if (n_it->second.pos_in_incons != 0) { // currently in INCONS, move to OPEN
            incons.erase(n_it->second.pos_in_incons);
            n_it->second.pos_in_incons = 0;
            n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
          } else if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
            open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
          } else { // in INCONS nor OPEN
            n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
          }
				}
	    }
		}
	} 
  return false;
}

inline void AStarInconsOpt() {
  eps = 10000;
  
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);
  
  while(!open.empty()) {
    if (improvePathInconsOpt()) { // better path found
      // move states from open into incons and resort incons with new e-value (prune states with e-value <= 1)
      double epsprime = 1;
      for (size_t i = 0; i < incons.size(); ) {
        if (G / (double) (incons[i]->second.g + incons[i]->second.h) > epsprime) {
          epsprime = G / (double) (incons[i]->second.g + incons[i]->second.h);
        }

        if (e(incons[i]) <= 1) {
          incons[i]->second.pos_in_incons = 0;
          incons.erase_unsafe(i);
        } else {
          incons.changeKey_unsafe(i, std::make_pair(-e(incons[i]), incons.key(i).second));
          ++i;
        }
      }
      for (size_t i = 0; i < open.size(); ++i) {
        if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
          epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
        }

        open[i]->second.pos_in_open = 0;
        if (e(open[i]) > 1) {
          open[i]->second.pos_in_incons = incons.insert_unsafe(std::make_pair(-e(open[i]), open.key(i).second), open[i]);
        }
      }
      open.clear();
      incons.heapify();

      printf_s("%24.24g \n", eps);
      publishSolution(epsprime);
    }
    	  
    // move first state of incons onto open and set epsilon
    if (!incons.empty()) {
      if (-incons.key(0).first < eps) {
        eps = -incons.key(0).first;
        //std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
      }
      while (!incons.empty() && -incons.key(0).first >= eps) {
        incons.front()->second.pos_in_open = open.insert(std::make_pair(f(incons.front()), incons.key(0).second), incons.front());
        incons.front()->second.pos_in_incons = 0;
        incons.pop();
      }
    }
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}

inline bool improvePathARAStar() {
  ++iterationNumber;

  while (!open.empty() && open.key(0).first < G) {
	   if(((clock() - startTime) / (double) CLOCKS_PER_SEC) > 2000) {
		  std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
		  exit(1);
	  }
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest f-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    // place that state s onto the closed list
		s_it->second.closed = iterationNumber;
    
    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
      
        // put or update n in open or incons
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        if (n_it->second.closed == iterationNumber) { // Closed
          // Push on incons
          if (n_it->second.pos_in_incons != 0) { // already in INCONS, update f-value
            incons.changeKey_unsafe(n_it->second.pos_in_incons->pos, std::make_pair(0, --open_op));
          } else {
            n_it->second.pos_in_incons = incons.insert_unsafe(std::make_pair(0, --open_op), n_it);
          }
				} else { // Not closed
          // Push in in Open list
          if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
            open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
          } else { // not in OPEN
            n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
          }
				}
	    }
		}
	} 
  return false;
}

inline void ARAStar(double initeps, double decreps) {
  eps = initeps;
  
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);
  
  while(!open.empty()) {
    bool result = improvePathARAStar();

    std::cout << "Epsilon: " << eps << " Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
    
    eps -= decreps;
    
    // move states from incons into open and resort open with new f-value (prune states with e-value <= 1)
    double epsprime = 1;
    for (size_t i = 0; i < open.size(); ) {
      if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
        epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
      }
      if (e(open[i]) <= 1) {
        open[i]->second.pos_in_open = 0;
        open.erase_unsafe(i);
      } else {
        open.changeKey_unsafe(i, std::make_pair(f(open[i]), open.key(i).second));
        ++i;
      }
    }
    for (size_t i = 0; i < incons.size(); ++i) {
      if (G / (double) (incons[i]->second.g + incons[i]->second.h) > epsprime) {
        epsprime = G / (double) (incons[i]->second.g + incons[i]->second.h);
      }
      if (e(incons[i]) > 1) {
        incons[i]->second.pos_in_open = open.insert_unsafe(std::make_pair(f(incons[i]), incons.key(i).second), incons[i]);
      }
      incons[i]->second.pos_in_incons = 0;
    }
    incons.clear();
    open.heapify();

    if (result) { // better path found
      
		publishSolution(eps);
    }
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}


inline bool improvePathAnytimeAStar() {
  while (!open.empty()) { // && G >= open.key(0).first) {
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && s_it->second.g + c < n_it->second.g) { 
        // put or update n in open
        n_it->second.g = s_it->second.g + c;
        n_it->second.bp = s_it;

        // Push in in Open list
        if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
          open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
        } else { // not in OPEN
          n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
        }
				
	    }
		}
	} 
  return false;
}

inline void AnytimeAStar(double initeps) {
  eps = initeps;
 
  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
  start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);
  
  while(!open.empty()) {
    bool result = improvePathAnytimeAStar();

    // prune states from open with e-value <= 1
    double epsprime = 1;
    for (size_t i = 0; i < open.size(); ) {
      if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
        epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
      }
      if (e(open[i]) <= 1) {
        open[i]->second.pos_in_open = 0;
        open.erase_unsafe(i);
      } else {
        ++i;
      }
    }
    open.heapify();

    if (result) {
      publishSolution(epsprime);
    }
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}


inline bool improvePathRWAStar() {
  ++iterationNumber;

  while (!open.empty()) { // && open.key(0).first < G) {
    std::map<Coord, Cell>::iterator s_it = open.front();
    open.pop();  // remove state with largest e-value from OPEN
    s_it->second.pos_in_open = 0;
		
    if (isGoal(s_it->first)) {
      G = s_it->second.g;
      goal_it = s_it;
      return true;
    }

    ++count; // count of expanded states

    std::vector<std::pair<Coord, int> > neighbors;
    createNeighbors(s_it->first, neighbors);

    for (size_t i = 0; i < neighbors.size(); ++i) {
      const Coord& n = neighbors[i].first;
      const int& c = neighbors[i].second;  // c is the cost to go from one cell to another. The cells are always adjacent
     
      std::map<Coord, Cell>::iterator n_it = env.find(n);
      if (n_it == env.end()) {
        Cell cell;
        //cell.traversable = collisionCheck(n);
        cell.h = h(n);
        cell.closed = INFTY;
        n_it = env.insert(std::make_pair(n,cell)).first;
      }

      if (n_it->second.traversable && s_it->second.g + c + n_it->second.h < G && (s_it->second.g + c < n_it->second.g || n_it->second.closed < iterationNumber)) { 
        // put or update n in open
        if (s_it->second.g + c < n_it->second.g) {
          n_it->second.g = s_it->second.g + c;
          n_it->second.bp = s_it;
        }
        n_it->second.closed = iterationNumber; // closed takes role of ``seen''
               
        // Push in in Open list
        if (n_it->second.pos_in_open != 0) { // already in OPEN, update f-value
          open.decreaseKey(n_it->second.pos_in_open, std::make_pair(f(n_it), --open_op));
        } else { // not in OPEN
          n_it->second.pos_in_open = open.insert(std::make_pair(f(n_it), --open_op), n_it);
        }
				
	    }
		}
	} 
  return false;
}

inline void RWAStar(double initeps, double decreps) {
  eps = initeps;

  Cell cell;
  cell.g = 0;
  cell.h = h(start);
  cell.bp = env.end();

  std::map<Coord, Cell>::iterator start_it = env.insert(std::make_pair(start, cell)).first;
 
  while(eps >= 1) {
    start_it->second.closed = iterationNumber + 1;
    start_it->second.pos_in_open = open.insert(std::make_pair(f(start_it), open_op), start_it);

    bool result = improvePathRWAStar();

    std::cout << "Epsilon: " << eps << "  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;

    double epsprime = 1;
    for (size_t i = 0; i < open.size(); ++i) {
      if (G / (double) (open[i]->second.g + open[i]->second.h) > epsprime) {
        epsprime = G / (double) (open[i]->second.g + open[i]->second.h);
      }
      open[i]->second.pos_in_open = 0;
    }

    if (result) {
      publishSolution(epsprime);
    }

    eps -= decreps;

    open.clear();
	}
  std::cout << "Epsilon: 1  Time: " << (clock() - startTime) / (double) CLOCKS_PER_SEC << std::endl;
}


int _tmain(int argc, _TCHAR* argv[])
{
  init();

  startTime = clock();
  
  //AStarEKey();
  //AStarInconsOpt();
  ARAStar(1.30, 0.01);
  //AnytimeAStar(10);
  //RWAStar(2, 0.2);


  return 0;
}
  